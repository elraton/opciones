from django.http import HttpResponseNotFound
from django.shortcuts import render
from django.views.generic import TemplateView

from company.models import Company
from news.models import News


class NewsView(TemplateView):
    template_name = 'news/news.html'

    def get(self, request, *args, **kwargs):
        try:
            news = News.objects\
                .select_related('category', 'autor')\
                .all()\
                .order_by('-id')[:4]
            companies = Company.objects.all()
            if news:
                data = {
                    'news': news,
                    'companies': companies,
                }
                return render(request, self.template_name, data)
            else:
                return HttpResponseNotFound("not found")
        except Exception as e:
            return HttpResponseNotFound("not found")
