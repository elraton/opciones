from django.http import HttpResponseNotFound
from django.shortcuts import render
from django.views.generic import TemplateView

from company.models import Company
from news.models import News


class NewsItemView(TemplateView):
    template_name = 'news/news_item.html'

    def get(self, request, *args, **kwargs):
        slug = kwargs.get('url', None)
        if slug:
            try:
                news = News.objects \
                    .select_related('category', 'autor') \
                    .filter(slug=slug).first()
                news_related = News.objects\
                    .select_related('category', 'autor')\
                    .all()\
                    .exclude(id=news.id)\
                    .order_by('-id')[:3]
                companies = Company.objects.all()
                if news:
                    data = {
                        'news': news,
                        'companies': companies,
                        'news_related': news_related
                    }
                    return render(request, self.template_name, data)
                else:
                    return HttpResponseNotFound("not found")
            except Exception as e:
                return HttpResponseNotFound("not found")
        else:
            return HttpResponseNotFound("not found")
