import logging
import os
import uuid

from PIL import Image
from django.core.management import BaseCommand
from django.db.transaction import atomic
from django.utils.text import slugify

from company.models import Slider, Company
from news.models import Category, Autor, News
from opciones.settings import BASE_DIR

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        try:
            with atomic():
                mode = 0o777
                try:
                    os.mkdir('{}/news'.format(os.path.join(BASE_DIR, 'media')), mode)
                except Exception as e:
                    pass
                category1 = Category()
                category1.name = 'ciencia y tecnología'
                category1.save()

                autor1 = Autor()
                autor1.names = 'Nombre'
                autor1.last_names = 'Apellido'
                autor1.save()

                news1 = News()
                news1.title = '¿Qué es y cómo aprovechar el lanzamiento de un proyecto?'
                news1.slug = slugify(news1.title)
                news1.category = category1
                news1.autor = autor1

                image_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'news1.jpg')
                image = Image.open(image_file)
                new_name = 'news/{}.jpg'.format(uuid.uuid4())
                new_url = '{}/{}'.format(os.path.join(BASE_DIR, 'media'), new_name)
                image.save(new_url)
                news1.image = new_name

                news1.content = "<p>Empezamos el a&ntilde;o con un art&iacute;culo sobre las tendencias que vamos a encontrar en el 2021 en el sector de la construcci&oacute;n. Estas vienen marcadas, sobre todo por la llegada en el 2020, y a&uacute;n presente, epidemia mundial. La sostenibilidad, las nuevas tecnolog&iacute;as y la arquitectura modular ser&aacute;n los principales protagonistas.</p>" \
                                "<p><strong>SOSTENIBILIDAD</strong></p>"\
                                "<p>Si ya hab&iacute;a habido un aumento en la concienciaci&oacute;n medio ambiental, tras el confinamiento y como este ha afectado positivamente al medio ambiente, a&uacute;n somos m&aacute;s conscientes de la necesidad inmediata de tomar medidas en todos los &aacute;mbitos.</p>" \
                                "<p>En el sector de la arquitectura y construcci&oacute;n ya se llevan a&ntilde;os tomando medidas al respeto, entre las cuales tendr&aacute;n m&aacute;s peso este a&ntilde;o:</p>"\
                                "<p><strong>CERTIFICADOS DE CONSTRUCCI&Oacute;N SOSTENIBLE</strong></p>" \
                                "<p>Para empezar, encontramos los Certificados de Construcci&oacute;n Sostenible, a los que cada vez se les da m&aacute;s importancia y se est&aacute;n convirtiendo en imprescindibles para toda edificaci&oacute;n. En Espa&ntilde;a los primordiales son:</p>" \
                                "<p>Est&aacute;ndar Passivhaus: va enfocado &uacute;nicamente a las t&eacute;cnicas de edificaci&oacute;n. Considera la optimizaci&oacute;n de los recursos disponibles mediante las t&eacute;cnicas pasivas de construcci&oacute;n.</p>" \
                                "<p>Por ejemplo, aplicar una buena orientaci&oacute;n de las ventanas para aprovechar al m&aacute;ximo la luz natural.</p>" \
                                "<p>Clasificaci&oacute;n BREEM: su funci&oacute;n es detectar el grado de adaptabilidad regional y la modificaci&oacute;n del sistema a las particularidades legislativas y naturales de la zona.</p>" \
                                "<p>Sistema LEED: eval&uacute;a el edificio a lo largo de todo su ciclo de vida, desde el planteamiento urban&iacute;stico, hasta la construcci&oacute;n y su mantenimiento.</p>" \
                                "<p>Sistema VERDE: este se basa en el C&oacute;digo T&eacute;cnico de la Edificaci&oacute;n y las Directivas Europeas. Tiene en cuenta la reducci&oacute;n del impacto medioambiental del edificio con respecto a otro de referencia.</p>" \
                                "<p><strong>EDIFICIOS DE CONSUMO DE ENERG&Iacute;A CASI NULO</strong></p>" \
                                "<p>Por otro lado, cada vez son m&aacute;s populares los Edificios de Consumo de Energ&iacute;a casi Nulo, construcciones con un gasto el&eacute;ctrico muy bajo y derivado de fuentes renovables.</p>" \
                                "<p>Este a&ntilde;o se vuelve un indispensable para los arquitectos ya que entra en vigor una nueva normativa energ&eacute;tica que obliga a todo espa&ntilde;ol que tenga como propiedad un edificio de nueva planta, a que este cumpla con las caracter&iacute;sticas de los nZEB (nearly Zero Energy Buildings).&nbsp;</p>" \
                                '<p><img alt="" src="/media/uploads/2021/10/01/news-image-cont_Eo4x9Vf.jpg" style="height:523px; width:1046px" /><br />' \
                                '<span style="color:#606060"><strong>Imagen: Materiales sostenibles.</strong></span></p>' \
                                "<p><strong>MATERIALES SOSTENIBLES</strong></p>" \
                                "<p>En el sector de la sostenibilidad, y dado el estado de alarma medio ambiental que tenemos desde ya hace unos a&ntilde;os, se est&aacute; promoviendo un gran esfuerzo en la investigaci&oacute;n de nuevos materiales que den mejor rendimiento t&eacute;rmico, m&aacute;s duraderos y que generen menos residuos.</p>" \
                                "<p>Esto nos ha tra&iacute;do al mercado materiales nuevos como el cemento vivo, el Smartwood, el hierro verde o un nuevo pol&iacute;mero de goma hecho a partir de pl&aacute;stico reciclado.</p>" \
                                "<p>Precisamente la reutilizaci&oacute;n de productos/materiales usados es una de las grandes tendencias en cuanto a materiales y sostenibilidad. Con ello se pretende disminuir los desperdicios y otorgar una segunda vida a los ya usados.</p>" \
                                "<p><strong>TECNOLOG&Iacute;A</strong></p>" \
                                "<p>La tecnolog&iacute;a est&aacute; ya integrada en nuestras vidas y cada vez alcanza mayor envergadura. As&iacute; que en la construcci&oacute;n no iba a ser menos.</p>" \
                                "<p><strong>DOM&Oacute;TICA</strong></p>" \
                                "<p>La Covid19 y su alto porcentaje de contagio nos genera la necesidad de tocar cuantas menos cosas mejor y la dom&oacute;tica es perfecto para ello. Creemos que &eacute;sta tendr&aacute; un aumento considerable durante el 2021, sobre todo en espacios p&uacute;blicos como son los gimnasios, museos, oficinas, centros comerciales, etc.</p>" \
                                "<p>El reconocimiento facial y la dom&oacute;tica son algunas de las soluciones para conseguir que las personas no tengan contacto directo con elementos que puedan ser focos de virus y bacterias. Adem&aacute;s, la dom&oacute;tica tambi&eacute;n puede controlar otros factores como la temperatura, la humedad o las concentraciones de CO2 y otros contaminantes.</p>" \
                                "<p><strong>ROB&Oacute;TICA</strong></p>" \
                                "<p>&iexcl;Los robots entran en la industria de la construcci&oacute;n! Desde los que colocan ladrillos hasta los que los crean. Esto est&aacute; consiguiendo mejorar los tiempos de construcci&oacute;n y obtener m&aacute;s calidad en los resultados.</p>" \
                                "<p>Adem&aacute;s, la rob&oacute;tica en la arquitectura aporta una edificaci&oacute;n m&aacute;s sostenible y con menos coste econ&oacute;mico. No se trata de sustituir la labor humana, sino de complementarla.</p>" \
                                "<p>Podemos encontrar robots que aplican pintura; que colocan los ladrillos a mayor velocidad y con m&aacute;s precisi&oacute;n que el ojo humano; demoledores de edificios (aunque estos no son m&aacute;s r&aacute;pidos que los tradicionales, s&iacute; que aportan mayor seguridad); para el mantenimiento de edificios y la limpieza de ventanas; y muchas m&aacute;s funciones que d&iacute;a a d&iacute;a van surgiendo.</p>" \
                                "<p>Y tambi&eacute;n tenemos las impresoras 3D, las cuales crean piezas de construcci&oacute;n prefabricadas (que pueden incluso ensamblar), as&iacute; como mobiliario o fachadas. Todo ello dando la opci&oacute;n de crearlo in situ o en fabrica.</p>"
                news1.save()

                self.stdout.write(self.style.SUCCESS('success'))
        except Exception as e:
            logger.error(e)
            self.stdout.write(self.style.ERROR(e))
