from django.urls import path
from .views import NewsView, NewsItemView

urlpatterns = [
    path('', NewsView.as_view(), name='news'),
    path('<slug:url>/', NewsItemView.as_view(), name='news'),
]
