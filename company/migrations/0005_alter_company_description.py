# Generated by Django 3.2.7 on 2021-09-28 03:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0004_company_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='description',
            field=models.TextField(blank=True, help_text='descripcion para home y primer bloque menor tamaño', null=True),
        ),
    ]
