from .sliders import Slider
from .aboutus import AboutUs
from .mision import Mision
from .vision import Vision
from .social_responsability import SocialResponsability
